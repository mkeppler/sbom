# Eclipse Foundation SBOM Generation

We use this project to capture information regarding SBOM generation by Eclipse Projects.

Here, you will find documentation, best practices, and pointers to policies and other materials that help Eclipse Project teams generate SBOM and make them available to adopters. 

## Documentation

* [Eclipse Project SBOM Documentation](./docs/sbom.adoc) 
  * [Maven-based Java Projects](./docs/sbom.adoc#sbom-maven)
  * [Node-based Projects](./docs/sbom.adoc#sbom-node)

Note that the information that we're assembling here may eventually be moved to more appropriate locations (e.g., the [Eclipse Project Handbook](https://www.eclipse.org/projects/handbook)).

### Tutorials 

* [Building EE4J SBOMs with he cyclonedx-maven-plugin](./docs/howto_build_sbom_wtih_cyclondx-maven-plugin.adoc)

## Eclipse Projects that Build SBOMs

This list shows latest releases only. Artifacts are identified using their Package URL (purl) ID.

* [Eclipse Dash](https://projects.eclipse.org/projects/technology.dash)
  * pkg:maven/org.eclipse.dash/org.eclipse.dash.licenses@1.0.2?type=jar [CycloneDX](https://repo.eclipse.org/service/local/artifact/maven/redirect?r=dash-licenses-releases&g=org.eclipse.dash&a=org.eclipse.dash.licenses&v=1.0.2&e=json&c=cyclonedx)
  * pkg:maven/org.eclipse.dash/license-tool-plugin@1.0.2?type=maven-plugin [CycloneDX](https://repo.eclipse.org/service/local/artifact/maven/redirect?r=dash-licenses-releases&g=org.eclipse.dash&a=license-tool-plugin&v=1.0.2&e=json&c=cyclonedx)
  * pkg:maven/org.eclipse.dash/eclipse-api-for-java@1.0.0?type=jar [CycloneDX](https://repo.eclipse.org/service/local/artifact/maven/redirect?r=dash-licenses-releases&g=org.eclipse.dash&a=eclipse-api-for-java&v=1.0.0&e=json&c=cyclonedx)

## Eclipse Contributor Agreement

In order to be able to contribute to Eclipse Foundation projects you must
electronically sign the Eclipse Contributor Agreement (ECA).

* https://www.eclipse.org/legal/ECA.php

The ECA provides the Eclipse Foundation with a permanent record that you agree
that each of your contributions will comply with the commitments documented in
the Developer Certificate of Origin (DCO). Having an ECA on file associated with
the email address matching the "Author" field of your contribution's Git commits
fulfills the DCO's requirement that you sign-off on your contributions.

For more information, please see the Eclipse Committer Handbook:
https://www.eclipse.org/projects/handbook/#resources-commit

## License

Unless otherwise specified, all content is made available under the terms of the Eclipse Public License 2.0 (EPL-2.0).
